import 'package:flutter/material.dart';

class CardBodyCoretan extends StatelessWidget {
  final String image;
  final String like;
  final String comment;
  final String ink;
  final String caption;
  const CardBodyCoretan({
    Key? key,
    required this.image,
    required this.like,
    required this.comment,
    required this.ink,
    required this.caption,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.all(8.0),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Stack(children: [
            Container(
              height: 200.00,
              decoration: BoxDecoration(
                image: DecorationImage(
                  image: AssetImage(image),
                  fit: BoxFit.cover,
                ),
                borderRadius: BorderRadius.circular(10),
              ),
            ),
            Positioned(
              right: 0,
              child: Container(
                // margin: EdgeInsets.only(right: 10, top: 10),
                constraints: const BoxConstraints(minWidth: 50, maxWidth: 70),
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(50),
                  color: const Color.fromARGB(151, 197, 193, 193),
                ),
                child: Padding(
                  padding: const EdgeInsets.all(4.0),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      const ImageIcon(
                        AssetImage('assets/icons/watch.png'),
                        color: Colors.white,
                      ),
                      Container(
                        margin: const EdgeInsets.only(left: 4),
                        child: const Text(
                          "10",
                          style: TextStyle(color: Colors.white),
                        ),
                      )
                    ],
                  ),
                ),
              ),
            )
          ]),
          Container(
            margin: const EdgeInsets.only(top: 10),
            child: Row(
              children: [
                Container(
                  margin: const EdgeInsets.only(right: 10),
                  child: Row(
                    children: [
                      const Icon(
                        Icons.favorite_border_outlined,
                        color: Color(0xFFF9725C),
                      ),
                      Container(
                        margin: const EdgeInsets.only(left: 5),
                        child: Text(
                          like,
                          style: const TextStyle(color: Color(0xFF7A7A7A)),
                        ),
                      )
                    ],
                  ),
                ),
                Container(
                  margin: const EdgeInsets.only(right: 10),
                  child: Row(
                    children: [
                      const ImageIcon(
                        AssetImage('assets/icons/comment.png'),
                        color: Color(0xFF7A7A7A),
                      ),
                      Container(
                        margin: const EdgeInsets.only(left: 5),
                        child: Text(
                          comment,
                          style: const TextStyle(color: Color(0xFF7A7A7A)),
                        ),
                      )
                    ],
                  ),
                ),
                Container(
                  margin: const EdgeInsets.only(right: 10),
                  child: Row(
                    children: [
                      const ImageIcon(AssetImage("assets/icons/ink.png")),
                      Container(
                        margin: const EdgeInsets.only(left: 5),
                        child: Text(
                          ink,
                          style: const TextStyle(color: Color(0xFF7A7A7A)),
                        ),
                      )
                    ],
                  ),
                ),
                const Spacer(),
                const Align(
                  alignment: Alignment.centerLeft,
                  child: ImageIcon(
                    AssetImage('assets/icons/bookmark.png'),
                    color: Color(0xFF3D3D3D),
                  ),
                ),
              ],
            ),
          ),
          Container(
            margin: const EdgeInsets.only(top: 10),
            child: Text(
              caption,
              style: const TextStyle(
                fontWeight: FontWeight.w500,
                fontSize: 12,
              ),
            ),
          ),
          Container(
            margin: const EdgeInsets.only(top: 10),
            child: const Text(
              '5 menit yang lalu',
              style: TextStyle(
                fontWeight: FontWeight.w500,
                fontSize: 10,
                color: Color(0xFF7A7A7A),
              ),
            ),
          ),
        ],
      ),
    );
  }
}
