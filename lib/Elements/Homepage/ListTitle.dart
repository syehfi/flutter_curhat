import 'package:flutter/material.dart';

class ListTitle extends StatelessWidget {
  final String text;
  final String avatar;
  final String gender;
  const ListTitle({
    Key? key,
    required this.text,
    required this.avatar,
    required this.gender,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return ListTile(
      leading: Stack(
        children: [
          Image.asset(avatar)
          // CircleAvatar(
          //   backgroundImage: AssetImage(avatar),
          //   radius: 25,
          // ),
          // Positioned(
          //   right: 0,
          //   left: 0,
          //   bottom: -15,
          //   child: Container(
          //     width: 28,
          //     height: 28,
          //     decoration: const BoxDecoration(
          //       shape: BoxShape.circle,
          //     ),
          //     child: Padding(
          //       padding: const EdgeInsets.all(2), // border width
          //       child: Container(
          //         // or ClipRRect if you need to clip the content
          //         decoration: const BoxDecoration(
          //           shape: BoxShape.circle,
          //           color: Colors.white, // inner circle color
          //         ),
          //         child: Container(
          //           child: ImageIcon(
          //             AssetImage(gender),
          //             color: Colors.blue,
          //           ),
          //         ), // inner content
          //       ),
          //     ),
          //   ),
          // )
        ],
      ),
      title: Container(
        margin: const EdgeInsets.symmetric(horizontal: 10),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Container(
              margin: const EdgeInsets.only(bottom: 5),
              child: Text(
                text,
                style: const TextStyle(fontSize: 12),
              ),
            ),
            Row(
              children: [
                Container(
                  margin: const EdgeInsets.only(right: 10),
                  child: Row(
                    children: const [
                      ImageIcon(
                        AssetImage('assets/icons/public.png'),
                        color: Color(0xFFB8B8B8),
                      ),
                      Text(
                        "Publik",
                        style: TextStyle(
                          color: Color(0xFFB8B8B8),
                          fontSize: 10,
                        ),
                      ),
                    ],
                  ),
                ),
                Container(
                  margin: const EdgeInsets.only(right: 10),
                  child: Row(
                    children: const [
                      ImageIcon(
                        AssetImage('assets/icons/looks.png'),
                        color: Color(0xFFB8B8B8),
                      ),
                      Text(
                        "Buku Harian",
                        style: TextStyle(
                          color: Color(0xFFB8B8B8),
                          fontSize: 10,
                        ),
                      ),
                    ],
                  ),
                ),
              ],
            )
            // Container(
            //   padding: const EdgeInsets.fromLTRB(10, 5, 10, 5),
            //   decoration: BoxDecoration(
            //     borderRadius: BorderRadius.circular(100),
            //     border: Border.all(
            //       color: const Color(0xFFD76800),
            //     ),
            //   ),
            //   child: const Text(
            //     'Buku Harian',
            //     style: TextStyle(
            //       color: Color(0xFFD76800),
            //     ),
            //   ),
            // )
          ],
        ),
      ),
      trailing: const Icon(Icons.more_vert),
    );
  }
}
