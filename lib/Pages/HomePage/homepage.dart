import 'package:curhat_app/Components/HomePage/Coretan.dart';
import 'package:curhat_app/Components/HomePage/Ucapan.dart';
import 'package:flutter/material.dart';

class HomePage extends StatelessWidget {
  const HomePage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return DefaultTabController(
      length: 2,
      child: Scaffold(
        appBar: AppBar(
          backgroundColor: Colors.white,
          shadowColor: Colors.transparent,
          title: Image.asset(
            'assets/icons/curhat.png',
            fit: BoxFit.cover,
            width: 150,
          ),
          actions: [
            IconButton(
              icon: Stack(
                children: const [
                  Icon(
                    Icons.notifications_outlined,
                    color: Color(0xFF3D3D3D),
                  ),
                ],
              ),
              onPressed: () {
                // do something
              },
            ),
            IconButton(
              icon: const Icon(
                Icons.navigation_outlined,
                color: Color(0xFF3D3D3D),
              ),
              onPressed: () {
                // do something
              },
            ),
          ],
        ),
        backgroundColor: Colors.white,
        body: Column(
          children: const [
            TabBar(
              indicatorColor: Color(0xFFFB8500),
              indicatorWeight: 5,
              labelColor: Color(0xFFFB8500),
              unselectedLabelColor: Color(0xFFCCCCCC),
              labelStyle: TextStyle(
                fontSize: 18,
                fontWeight: FontWeight.w600,
              ),
              tabs: [
                Tab(
                  text: "Coretan",
                ),
                Tab(
                  text: "Ucapan",
                )
              ],
            ),
            Expanded(
              child: TabBarView(
                children: [
                  CardCoretan(),
                  CardUcapan(),
                ],
              ),
            ),
          ],
        ),
        floatingActionButton: FloatingActionButton(
          onPressed: () {
            // Add your onPressed code here!
          },
          backgroundColor: const Color(0xFFFB8500),
          child: Row(
            crossAxisAlignment: CrossAxisAlignment.center,
            mainAxisAlignment: MainAxisAlignment.center,
            children: const [
              Text('+'),
              ImageIcon(
                AssetImage('assets/icons/ink.png'),
              )
            ],
          ),
        ),
      ),
    );
  }
}
