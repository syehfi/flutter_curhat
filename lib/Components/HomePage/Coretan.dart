import 'package:curhat_app/Elements/Homepage/CardBodyCoretan.dart';
import 'package:curhat_app/Elements/Homepage/ListTitle.dart';
import 'package:flutter/material.dart';

class CardCoretan extends StatelessWidget {
  const CardCoretan({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return SingleChildScrollView(
      child: Column(
        children: [
          Card(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: const [
                ListTitle(
                  avatar: 'assets/avatar/avatar1.png',
                  text: "Mantan Gamon",
                  gender: 'assets/icons/male.png',
                ),
                CardBodyCoretan(
                  image: 'assets/images/gambar1.jpg',
                  comment: '10',
                  ink: '3',
                  like: '20',
                  caption:
                      'Jodoh adalah cerminan dari diri kita. Kalau kita masih seperti ini, bagaimana kita bisa m ... lainnya',
                ),
              ],
            ),
          ),
          Card(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: const [
                ListTitle(
                  avatar: 'assets/avatar/avatar1.png',
                  text: "Anabel Baik",
                  gender: 'assets/icons/male.png',
                ),
                CardBodyCoretan(
                  image: 'assets/images/gambar1.jpg',
                  comment: '10',
                  ink: '0',
                  like: '20',
                  caption:
                      'Jadi dewasa itu ternyata berat ya, apa-apa harus kuat ngadepin kegiatan dengan punya ... lainnya',
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }
}
