import 'package:curhat_app/Elements/Homepage/CardBodyUcapan.dart';
import 'package:curhat_app/Elements/Homepage/ListTitle.dart';
import 'package:flutter/material.dart';

class CardUcapan extends StatelessWidget {
  const CardUcapan({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return SingleChildScrollView(
      child: Column(
        children: [
          Card(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: const [
                ListTitle(
                  avatar: 'assets/avatar/avatar1.png',
                  text: "Mantan Gamon",
                  gender: 'assets/icons/female.png',
                ),
                CardBodyUcapan(
                  image: 'assets/images/gambar1.jpg',
                  comment: '10',
                  ink: '3',
                  like: '20',
                  caption:
                      'Jodoh adalah cerminan dari diri kita. Kalau kita masih seperti ini, bagaimana kita bisa m ... lainnya',
                )
              ],
            ),
          )
        ],
      ),
    );
  }
}
