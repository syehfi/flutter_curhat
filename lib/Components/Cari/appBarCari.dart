import 'package:flutter/material.dart';

class AppBarCari extends StatelessWidget implements PreferredSizeWidget {
  const AppBarCari({Key? key}) : super(key: key);
  @override
  Size get preferredSize => const Size.fromHeight(100);

  @override
  Widget build(BuildContext context) {
    return AppBar(
      backgroundColor: Colors.white,
      shadowColor: Colors.transparent,
      title: TextField(),
      actions: [
        IconButton(
          icon: Stack(
            children: const [
              ImageIcon(
                AssetImage('assets/icons/filter.png'),
                color: Color(0xFF323232),
              )
              // Icon(
              //   Icons.filter,
              //   color: Color(0xFF3D3D3D),
              // ),
            ],
          ),
          onPressed: () {
            // do something
          },
        ),
      ],
    );
  }
}
